#coding:utf-8

# .     - Any Charcter Except New Line
# \d    - Digit (0-9)
# \D    - Not a Digit (0-9)
# \w    - word Character (a_z, A-Z, 0-9, _)
# \W    - Not a Word Character
# \s    - White Space (space, tab, newline)
# \S    - Not a White Space 

# \b    - Word Boundary
# \B    - Not a word Boundary
# ^     - Beginning of a string
# $     - End of a string

# []    - Matches Characters in brackets
# [^ ]  - Matches Characters NOT in brackets
# |     - Either Or
# ( )   - Group

# Quantifiers:
# *     - 0 or more
# +     - 1 or more
# ?     - 0 or 1
# {3}   - Exact Number
# {3,4} - Range of number (Min, Max)
import re
text_to_search = '''
abcdefghijklmnopqrstuvwxyz
ABCDEFGHIJKLMNOPQRSTUVWXYZ
1234567890

Ha HaHa

Metacharacters (Need to be escaped):
. ^ $ * + ? { } [ ] \ | ( )

gazoleen.com

06-87-03-58-89
06.87.03.58.89

M. SERREAU
M SERREAU
Melle SERREAU
Mme SERREAU
Monsieur SERREAU
Madame SERREAU
'''

sentence = 'Start a sentence and bring it to the end'

urls = '''
https://www.google.com
http://coreyms.com
https://youtube.com
https://www.nasa.gov
http://gazoleen.com
'''

print('\tTAB')
print(r'\tTAB')     #r = raw string

pattern = re.compile(r'abc')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'cba')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'.')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'\.')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'gazoleen\.com')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'\d')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'\bHa')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'\BHa')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'^Start')

matches = pattern.finditer(sentence)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'end$')

matches = pattern.finditer(sentence)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'\d\d')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match)

pattern = re.compile(r'\d\d.\d\d.\d\d.\d\d.\d\d')

matches = pattern.finditer(text_to_search)
print('')
for match in matches:

    print(match.group(0))

print('')
print('#Tous les numéroo 2 par 2 séparés par n\'importe quel caractère#')
pattern = re.compile(r'\d\d.\d\d.\d\d.\d\d.\d\d')
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.finditer(contents)
    for match in matches:
        print(match.group(0))

print('')
print('#Numéro séparé par .#')
pattern = re.compile(r'\d\d[\.]\d\d[\.]\d\d[\.]\d\d[\.]\d\d')
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.finditer(contents)
    for match in matches:
        print(match.group(0))

print('')
print('#Numéro séparé par /#')
pattern = re.compile(r'\d\d[\/]\d\d[\/]\d\d[\/]\d\d[\/]\d\d')
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.finditer(contents)
    for match in matches:
        print(match.group(0))

print('')
print('#Numéro démarrant par 06 ou 07#')
pattern = re.compile(r'0[67].\d\d.\d\d.\d\d.\d\d')
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.finditer(contents)
    for match in matches:
        print(match.group(0))

print('')
print('#Numéro de 2 digit en 2 digit#')
pattern = re.compile(r'\d{2}.\d{2}.\d{2}.\d{2}.\d{2}')
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.finditer(contents)
    for match in matches:
        print(match.group(0))

print('')
print('#M. #')
pattern = re.compile(r'M\.?\s')
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.finditer(contents)
    for match in matches:
        print(match.group(0))

print('')
print('#M. + le mot suivant#')
pattern = re.compile(r'M\.?\s[A-Z]\w*')
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.finditer(contents)
    for match in matches:
        print(match.group(0))

print('')
print('#Email#')
pattern = re.compile(r'[a-zA-Z0-9-_.]+@[a-zA-Z-_]+\.(com|edu|org|net|fr)')
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.finditer(contents)
    for match in matches:
        print(match.group(0))

print('')
print('#Email#')
pattern = re.compile(r'[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+') # Trouvée sur le WWW
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.finditer(contents)
    for match in matches:
        print(match.group(0))

print('')
print('#More regex with group#')

pattern = re.compile(r'https?://(www\.)?(\w+)(\.\w+)')

matches = pattern.finditer(urls)

for match in matches:
    print(match.group(0))

print('')
print('#More regex#')

pattern = re.compile(r'https?://(www\.)?(\w+)(\.\w+)')

subded_urls = pattern.sub(r'\2\3', urls)

print(subded_urls)

print('')
print('#Tous les numéroo 2 par 2 séparés par n\'importe quel caractère avec findall#')
pattern = re.compile(r'\d\d.\d\d.\d\d.\d\d.\d\d')
with open('fichier.txt', 'r', encoding = 'utf-8') as inputfile:
    contents = inputfile.read()
    matches = pattern.findall(contents) #Utulisation de findall() au lieu de finiter() Attention pas de reconnaissance de group
    for match in matches:
        print(match)