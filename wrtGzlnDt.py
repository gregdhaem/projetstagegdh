#coding:utf-8
import csv

delimiter_character = input('What character should be used as delimiter ? > ')

with open('outfile.csv', 'w', newline = '') as out_file:
    column_headers = ['Nom, Prénom', 'Email', 'Téléphone', 'Portable', 'Bureau', 'Adresse', 'Ville', 'Code Postal', 'Pays', 'Date du dernier RDV', 'Dernier RDV', 'Commentaire']
    gaz_data_writer = csv.DictWriter(out_file, fieldnames = column_headers, delimiter = delimiter_character)

    gaz_data_writer.writeheader()
    for i in range(1, 10):
        gaz_data_writer.writerow({'Nom, Prénom' : 'SERREAU Mikael', 'Email' : 'mikael@gazoleen.com', 'Téléphone' : '0186270021', 'Portable' : '0664521601', 'Bureau' : '', 'Adresse' : '10 avenue des Mondaults', 'Ville' : 'Floirac', 'Code Postal' : '33270', 'Pays' : 'France', 'Date du dernier RDV' : '16∕01∕2018', 'Dernier RDV' : 'Ramonage', 'Commentaire' : 'Passage tous les six mois'})
