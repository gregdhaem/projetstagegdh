#!/usr/bin/env python3
#coding:utf-8

import argparse # Parser for options, arguments and subcommands for CLI 
import os.path  # Pathname manipulation
import csv      # Reading and writing csv files
import re       # Operations with Regular Expression

class CleanFile:

    # Init variables
    nbr_of_columns              = ''
    infile                      = ''     # Using "infile" instead of "file" which seems to be a reserved word
    outfile                     = ''    
    mapping                     = {
        "name"                  : '', 
        "email"                 : '', 
        "phone"                 : '', 
        "cellphone"             : '', 
        "office"                : '', 
        "address"               : '', 
        "city"                  : '', 
        "zip_code"              : '', 
        "country"               : '', 
        "last_appointment_date" : '', 
        "appointment"           : '', 
        "comment"               : ''
    }


    def __init__(self):
        parser = argparse.ArgumentParser()  
        parser.add_argument('srcfile', help = 'Fichier CSV ')     
        parser.add_argument('outfile', help = 'Output CSV file')
        args = parser.parse_args()
        self.infile     = args.srcfile
        self.outfile    = args.outfile
        if not re.findall(r'\.csv', self.infile) or not os.path.isfile(self.infile): 
            print('ATTENTION : Vous devez importer un fichier CSV existant !')
            exit()
        self.dataset         = [line.strip().split(';') for line in open(self.infile)]
        self.nbr_of_columns  = len(self.dataset[0])

        # Display number of colums in input csv file
        print('INFO : Le fichier "{}" contient {} colonnes\n'.format(self.infile, self.nbr_of_columns))

        # Display header of input csv file
        print('INFO : La première ligne (entête) du fichier "{}" est la suivante : \n {}\n'.format(self.infile, self.dataset[0]))
        self.start()
        self._clean()


    def start(self):

        # User input questioning to map colums
        self.mapping["name"] = self._question("Quelle colonne contient le nom des clients ?")
        self.mapping["email"] = self._question("Quelle colonne contient le mail des clients ?")
        self.mapping["phone"] = self._question("Quelle colonne contient le n° de tél fixe des clients ?")
        self.mapping["cellphone"] = self._question("Quelle colonne contient le n° de tél portable des clients ?")
        self.mapping["office"] = self._question("Quelle colonne contient le n° de tél de bureau des clients ?")
        self.mapping["address"] = self._question("Quelle colonne contient l'adressse des clients ?")
        self.mapping["city"] = self._question("Quelle colonne contient la ville des clients ?")
        self.mapping["zip_code"] = self._question("Quelle colonne contient le code postal des clients ?")
        self.mapping["country"] = self._question("Quelle colonne contient le pays des clients ?")
        self.mapping["last_appointment_date"] = self._question("Quelle colonne contient la date du dernier RDV des clients ?")
        self.mapping["appointment"] = self._question("Quelle colonne contient le dernier RDV des clients ?")
        self.mapping["comment"] = self._question("Quelle colonne contient le commentaire des clients ?")
       

    def _question(self, question):
                   
        response = ''
        while True:
            response = input(question + " (1 à {}) >>> ".format(self.nbr_of_columns))
            response = re.sub(r'[^0-9]', '', response)
            if response and int(response) >= 1 and int(response) <= self.nbr_of_columns:
                confirm = ''
                while True:
                        confirm = input('La valeur que vous avez saisie est : " {} " \n\tMerci de confirmer (o/n) : '.format(response))
                        if confirm == 'o' or confirm == 'n':
                                break
                if confirm == 'o':
                        break

        print('\tVous avez confirmé la valeur suivante : " {} "\n'.format(response))

        return response

    def _clean(self):

        with open(self.infile, newline = '', encoding = 'utf-8') as flist:
            lines_read = csv.reader(flist, delimiter = ';', quoting = csv.QUOTE_ALL)

            line_count = 0
            with open(self.outfile, 'w', newline = '', encoding = 'utf-8') as olist:
                lines_write = csv.writer(olist, delimiter = ';', quoting = csv.QUOTE_ALL)
                for ln in lines_read:

                    if line_count == 0:     # Skip first header row
                        line_count += 1     # Go to first line of data after skiping first row
                    else:       

                        templine = []       # Initialize current temp line to empty  

####################
### Extract Name ##################################################################################
####################

                        if (ln[int(self.mapping['name']) - 1]) != '':
                            result = re.sub(r'[0-9]', '', ln[int(self.mapping['name']) - 1])
                            cleaned_result = re.sub(r'( )( ){1,}', ' ', result)
                            templine.insert(0, cleaned_result.strip())
                        else:
                            templine.insert(0, '')

#####################
### Extract Email #################################################################################
#####################

                        pattern = re.compile(r'[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+')
                        results = pattern.findall(ln[int(self.mapping['email']) - 1])
                        if results != []:
                            for cleaned_result in results:
                                templine.insert(1, cleaned_result)
                        else:
                            templine.insert(1, '')
                         
#####################
### Extract Phone #################################################################################
#####################

                        pattern = re.compile(r'\d{2}.{0,1}\d{2}.{0,1}\d{2}.{0,1}\d{2}.{0,1}\d{2}')
                        results = pattern.findall(ln[int(self.mapping['phone']) - 1])
                        if results != []:
                            for result in results:
                                cleaned_result = re.sub(r'[\s\.\-\/]', '', result)
                                templine.insert(2, cleaned_result)
                        else:
                            templine.insert(2, '')

##########################
### Extract Cell Phone ############################################################################
########################## 
                            
                        pattern = re.compile(r'\d{2}.{0,1}\d{2}.{0,1}\d{2}.{0,1}\d{2}.{0,1}\d{2}')
                        results = pattern.findall(ln[int(self.mapping['cellphone']) - 1])
                        if results != []:
                            for result in results:
                                cleaned_result = re.sub(r'[\s\.\-\/]', '', result)
                                templine.insert(3, cleaned_result)
                        else:
                            templine.insert(3, '')

############################
### Extract Office Phone ##########################################################################
############################

                        pattern = re.compile(r'\d{2}.{0,1}\d{2}.{0,1}\d{2}.{0,1}\d{2}.{0,1}\d{2}')
                        results = pattern.findall(ln[int(self.mapping['office']) - 1])
                        if results != []:
                            for result in results:
                                cleaned_result = re.sub(r'[\s\.\-\/]', '', result)
                                templine.insert(4, cleaned_result) 
                        else:
                            templine.insert(4, '')

#######################
### Extract Address ##################################################################################
#######################

                        templine.insert(5, ln[int(self.mapping['address']) - 1])

####################
### Extract City ##################################################################################
####################

                        templine.insert(6, ln[int(self.mapping['city']) - 1])

########################
### Extract Zip Code ##################################################################################
########################

                        pattern = re.compile(r'\d.{0,1}\d.{0,1}\d.{0,1}\d.{0,1}\d')
                        results = pattern.findall(ln[int(self.mapping['zip_code']) - 1])
                        if results != []:
                            for result in results:
                                cleaned_result = re.sub(r'[\s\.\-\/]', '', result)
                                templine.insert(6, cleaned_result)
                        else:
                            templine.insert(7, '')

#######################
### Extract Country ##################################################################################
#######################

                        templine.insert(8, ln[int(self.mapping['country']) - 1])

#####################################
### Extract Last Appointment Date ##################################################################################
#####################################

                        pattern = re.compile(r'(0[1-9]|[1-2][0-9]|3[0-1]).(0[1-9]|1[0-2]).(\d{4}|\d{2})')
                        results = pattern.findall(ln[int(self.mapping['last_appointment_date']) - 1])
                        if results != []:
                            for match in results:
                                if len(match[2]) == 2:
                                    cleaned_result = (match[0] + '/' + match[1] + '/20' + match[2])
                                elif len(match[2]) == 4:       
                                    cleaned_result = (match[0] + '/' + match[1] + '/' + match[2])
                                templine.insert(9, cleaned_result)

                        else:
                            templine.insert(9, '')

###########################
### Extract Appointment ##################################################################################
###########################

                        templine.insert(10, ln[int(self.mapping['appointment']) - 1])

#######################
### Extract Comment ##################################################################################
#######################

                        templine.insert(11, ln[int(self.mapping['comment']) - 1])


                        lines_write.writerow(templine)  # Write current row into templine
                        line_count += 1                 # Increment line number by 1
 
CleanFile()


