#!/usr/bin/env python3
#coding:utf-8

import argparse #Parser pour options, arguments et souscommandes pour les interfaces en ligne de commande 
import os.path  #Manipulation des pathname 
import csv      #Lecture et écriture des fichiers csv
import re       #Operations avec les expressions régulières

class CleanFile:
    infile = ''     #PB avec (srcfile: str = '') => [pylint] E0001:invalid syntax (<string>, line 10)
                    #Utilisation de infile plutôt que file qui semble être un mot réservé...
    outfile = ''    #idem que ligne du dessus avec avec (outfile: str = '')

    def __init__(self):
        parser = argparse.ArgumentParser()  #Objet por parser les string de ligne de commande en objet python
        parser.add_argument('srcfile', help = 'Input CSV file', type = str)     #méthode add_argument de argparse pour spécifier les opions en ligne de commande
        parser.add_argument('outfile', help = 'Output CSV file', type = str)
        args = parser.parse_args()
        self.infile = args.srcfile
        self.outfile = args.outfile
        if not re.findall(r'\.csv', self.infile) or not os.path.isfile(self.infile): #findall() retourne une liste de correpondance - isfile() vérifie que le path est un fichier
            print('WARNING: You must import an existing CSV file.')
            exit()

CleanFile()

